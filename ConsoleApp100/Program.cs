﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp100
{
    class Program
    {
        static void Main(string[] args)
        {
                        int a = 54;
            int b = 78;

            int max = a > b ? a : b;     // it's the same thing

            //if (a > b)
            //{
            //    Console.WriteLine(a);
            //}
            //else
            //{
            //    Console.WriteLine(b);
            //}

            Console.WriteLine(max);
 
            int a = 78;
            int b = 45;
            int max = Math.Max(a,b);
            Console.WriteLine(max);

            string a = "aaaaa";
            string b = "kkk";
           
            string max = a.Length > b.Length ? a : b;     // it's the same thing

            //if (a > b)
            //{
            //    Console.WriteLine(a);
            //}
            //else
            //{
            //    Console.WriteLine(b);
            //}

            Console.WriteLine($"max={max}");

            int[] numbers = { 1, 3, 2 };
            int max = numbers.Max();
            Console.WriteLine(max);

        }
    }
}
